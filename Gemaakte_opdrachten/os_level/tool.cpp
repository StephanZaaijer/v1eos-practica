#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[]){
	string input;
	string encrypted_input = "";
	getline(cin,input);
	int caesar_key;
	if (argc>1){
		caesar_key = atoi(argv[1])%26;
	}
	else{
		caesar_key = 5;
	}
	if(caesar_key==0){
		encrypted_input = input;
	}
	else{
		for(unsigned int i=0; i<input.length();i++){
			char character = input[i];
			int encrypted_character = character+caesar_key;
			if(character>'A' and character<'Z' or character>'a' and character<'z'){
				if(character <= 'Z'){
					if(encrypted_character>'Z'){
						encrypted_input+=encrypted_character-26;
					}
					else if(encrypted_character<'A'){
						encrypted_input+=encrypted_character+26;
					}
					else{
						encrypted_input+=encrypted_character;
					}
				}
				else{
					if(encrypted_character>'z'){
						encrypted_input+=encrypted_character-26;
					}
					else if(encrypted_character<'a'){
						encrypted_input+=encrypted_character+26;
					}
					else{
						encrypted_input+=encrypted_character;
					}
				}
			}
			else{
				encrypted_input+=encrypted_character;
			}
		}
	}
	cout << "Encrypted tekst:" << "\n"  << encrypted_input << "\n";
	return 0;
}
