#include "shell.hh"

int main(){ 
	std::string input;
	std::string prompt = "";

	int prompt_code = syscall(SYS_open, "promptinfo.txt", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
	char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
	while(syscall(SYS_read, prompt_code, byte, 1)){                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
		if (byte[0]=='\n'){
			break;
		}
		prompt.append(byte);  									//   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
	}
	syscall(SYS_close, prompt_code);
  while(true){
		std::cout << prompt;                   // Print het prompt
    	std::getline(std::cin, input);         // Lees een regel
    	if (input == "new_file") new_file();   // Kies de functie
    	else if (input == "ls") list();        //   op basis van
    	else if (input == "src") src();        //   de invoer
    	else if (input == "find") find();
    	else if (input == "seek") seek();
    	else if (input == "exit") return 0;
    	else if (input == "quit") return 0;
    	else if (input == "error") return 1;

	    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file(){
	const char* args_new_file[] = {NULL};
	std::string bestandsnaam;
	std::cout << "Geef de bestandsnaam:";
	getline(std::cin, bestandsnaam);
	args_new_file[0] = bestandsnaam.c_str();
	std::string string_bestandsinhoud = "";
	std::string input;
	int aantal_bytes = 0;
	std::cout << "Geef inhoud van bestand\n";
	while (getline(std::cin, input)){
		if (input!="<EOF>"){
			string_bestandsinhoud += input + "\n";
		}
	}
	aantal_bytes = string_bestandsinhoud.length();
	const char* bestandsinhoud[] = {NULL};
	bestandsinhoud[0] = string_bestandsinhoud.c_str();
	int file_descriptor = syscall(SYS_creat, args_new_file[0], 0644, NULL);
	syscall(SYS_write, file_descriptor, bestandsinhoud[0], aantal_bytes);
	syscall(SYS_close, file_descriptor);
	return;
	
}
void list(){
	int pid = syscall(SYS_fork);                             
	if (pid == 0){
		const char* args[] = { "/bin/ls", "-l", "-a", NULL };                            
		syscall(SYS_execve, args[0], args, NULL);
	}
	else{
		syscall(SYS_wait4, pid, NULL, NULL, NULL);
		return;
	}
}                       
void find() // ToDo: Implementeer volgens specificatie.
{
	std::cout << "Op welk woord wil je zoeken: ";
	std::string zoekwoord;
	std::cin >> zoekwoord;
	int fd[2];
	syscall(SYS_pipe, &fd, NULL);
	int pid = syscall(SYS_fork);
	if (pid==0){
		syscall(SYS_close, fd[0]);
		syscall(SYS_dup2, fd[1], 1);
		const char* args[] = { "/usr/bin/find", ".", NULL };
		int find = syscall(SYS_execve, args[0], args, NULL);
	}
	else if (pid>0){
		int pid2 = syscall(SYS_fork);
		if (pid2 == 0){
			syscall(SYS_close, fd[1]);
			syscall(SYS_dup2, fd[0], 0);
			const char* args2[] = { "/bin/grep", zoekwoord.c_str(), NULL};
			syscall(SYS_execve, args2[0], args2, NULL);
		}
		else if (pid2>0){
			syscall(SYS_close, fd[1]);
			syscall(SYS_close, fd[0]);
			syscall(SYS_wait4, pid, NULL, NULL, NULL);
			syscall(SYS_wait4, pid2, NULL, NULL, NULL);
			std::cin.ignore(1);
			return;
		}
		else{
			std::cout << "fork2 failed";
		}
	}
	else{
		std::cout << "fork failed";
	}
}
void seek(){
	const char* bestandsinhoud[] = {"x"};
	const char* bestandsopvulling[] = {"\0"};

	//Lseek
	int file_descriptor_seek = syscall(SYS_creat, "seek", 0644, NULL);
	syscall(SYS_write, file_descriptor_seek, bestandsinhoud , 1);
	syscall(SYS_lseek, file_descriptor_seek, 5242880, SEEK_END);
	syscall(SYS_write, file_descriptor_seek, bestandsinhoud , 1);
	syscall(SYS_close, file_descriptor_seek);

	//Loop
	int file_descriptor_loop = syscall(SYS_creat, "loop", 0644, NULL);
	syscall(SYS_write, file_descriptor_loop, bestandsinhoud , 1);
	for (int i=0; i<5242880; i++){
		syscall(SYS_write, file_descriptor_loop, bestandsopvulling , 1);
	}
	syscall(SYS_write, file_descriptor_loop, bestandsinhoud , 1);
	syscall(SYS_close, file_descriptor_loop);
	return;	
}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1)){                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
}
